Rails.application.routes.draw do

  root to: 'users#index'
  default_url_options :host => "http://localhost:3000"
  devise_for :users, :controllers => { omniauth_callbacks: 'omniauth_callbacks', path_names: { sign_in: "login", sign_out: "logout", :sign_up => "register" } }
  match '/users/:id/destroy', to: 'users#destroy', via: [:get, :patch], as: 'signout'
  resources :favourites
  # resources :tournaments, only: [:show] do
  #   collection do
  #     get :tournament_search
  #   end
  # end

  resources :comments
  resources :events
  resources :competitions
  resources :fan_cards
  resources :likes
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
