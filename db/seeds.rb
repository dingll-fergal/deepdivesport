# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# sports = ['Tennis', 'Formula 1', 'Football', 'Cricket', 'Boxing', 'Golf']
# sports.each do |sport|
#   Sport.create!(name: sport)
# end


url      = "https://restcountries.eu/rest/v1/all"
response = HTTParty.get(URI.encode(url))

response.each do |country|
  county      = Country.new
  county.name = country['name']
  county.iso  = country['alpha2Code']
  county.save!
end

sports = ['Football','Cricket', 'Formula 1' , 'Boxing']
sports.each do |sport|
  Sport.create!(name: sport)
end

EventDate.create(description: 'Testing', event_date: Time.now, event_date_sid: 'string', created_at: Time.now, updated_at: Time.now)
Competition.create(description: 'Laliga', sport_id: 1, event_date_id: 1, season_desc: 'this is testing', season_code: 'A12BZ', summer_season_flag: 'what', summer_season_desc: 'yes', summer_season_code: 'QW1Z', created_at: Time.now, updated_at: Time.now)
Competition.create(description: 'Champions Leauge', sport_id: 1, event_date_id: 1, season_desc: 'this is testing', season_code: 'A12BZ', summer_season_flag: 'what', summer_season_desc: 'yes', summer_season_code: 'QW1Z', created_at: Time.now, updated_at: Time.now)
Competition.create(description: 'Euorpion cup', sport_id: 1, event_date_id: 1, season_desc: 'this is testing', season_code: 'A12BZ', summer_season_flag: 'what', summer_season_desc: 'yes', summer_season_code: 'QW1Z', created_at: Time.now, updated_at: Time.now)
EventType.create(type_desc:'testing', sport_id: 1, competition_id: 1, country_id: 1, duration: 2, completed: 1, abandoned: 0, created_at: Time.now, updated_at: Time.now)
EventVenue.create(venue_name: 'Pakistan', home_team_id: 1, home_team_name: 'Pakistan', address1: 'Testting', address2: 'Testting', address3: 'Testting', country_id: 1, city: 'Lahore', postcode: 5400, phone: 123456789, capacity: 5000, owner: 'Fergal', created_at: Time.now, updated_at: Time.now)
Fixture.create(fixture_date: Time.now, home_ht: 'string', away_ht: 'string', result_ht: 'string', home_ft: 'string', away_ft: 'string', result_ft: 'string', raferee: 'string', home_shots: 5, away_shots: 5, target: 5, home_frees: 4, away_frees: 5, home_red_cards: 2, away_red_cards: 6, home_yellow_cards:7, away_yellow_cards:5, home_wides: 2, away_wides: 3, competition_id: 1, event_date_id: 1, home_team_id: 2, away_team_id:1, event_venue_id: 1, created_at: Time.now, updated_at: Time.now)
Event.create(description: 'Pak vs Ind', event_date_id: 1, event_type_id: 1, event_venue_id: 1, competition_id: 1, home_team_id: 2, away_team_id: 1, fixture_id: 1, created_at: Time.now, updated_at: Time.now)

