class CreateTeams < ActiveRecord::Migration[5.0]
  def change
    create_table :teams do |t|
      t.integer :sport_id
      t.string :name
      t.string :home_ground
      t.integer :country_id
      t.integer :team_position
      t.timestamps
    end
  end
end
