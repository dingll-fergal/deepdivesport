class CreateAdminProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :admin_profiles do |t|
      t.string :about
      t.string :phone
      t.integer :country_id
      t.string :photo_url

      t.timestamps
    end
  end
end
