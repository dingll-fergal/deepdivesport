class CreatePlayerFacts < ActiveRecord::Migration[5.0]
  def change
    create_table :player_facts do |t|

      t.integer :player_dim_id
      t.integer :competition_id
      t.integer :start_date
      t.boolean :is_current
      t.string :position

      t.timestamps
    end
  end
end
