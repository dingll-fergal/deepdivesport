class CreateFixtures < ActiveRecord::Migration[5.0]
  def change
    create_table :fixtures do |t|
      t.datetime :fixture_date
      t.string :home_ht
      t.string :away_ht
      t.string :result_ht
      t.string :home_ft
      t.string :away_ft
      t.string :result_ft
      t.string :raferee
      t.integer :home_shots
      t.integer :away_shots
      t.integer :target
      t.integer :away_shots
      t.integer :home_frees
      t.integer :away_frees
      t.integer :home_red_cards
      t.integer :away_red_cards
      t.integer :home_yellow_cards
      t.integer :away_yellow_cards
      t.integer :home_wides
      t.integer :away_wides
      t.integer :competition_id
      t.integer :event_date_id
      t.integer :home_team_id
      t.integer :away_team_id
      t.integer :event_venue_id

      t.timestamps
    end
  end
end
