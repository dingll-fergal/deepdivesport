class CreateTournamentTeams < ActiveRecord::Migration[5.0]
  def change
    create_table :tournament_teams do |t|
      t.integer :team_id

      t.timestamps
    end
  end
end
