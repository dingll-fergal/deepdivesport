class CreateFavourites < ActiveRecord::Migration[5.0]
  def change
    create_table :favourites do |t|
      t.integer :user_id
      t.integer :favouriteable_id
      t.string :favouriteable_type

      t.timestamps
    end
  end
end
