class AddUserIdToFanCard < ActiveRecord::Migration[5.0]
  def change
    add_column :fan_cards, :user_id, :integer
  end
end
