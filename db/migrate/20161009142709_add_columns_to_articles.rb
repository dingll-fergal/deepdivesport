class AddColumnsToArticles < ActiveRecord::Migration[5.0]
  def change
    remove_column :articles, :tournament_id
    add_column :articles, :competition_id, :integer
  end
end
