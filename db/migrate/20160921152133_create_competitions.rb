class CreateCompetitions < ActiveRecord::Migration[5.0]
  def change
    create_table :competitions do |t|
      t.string :description
      t.integer :sport_id
      t.integer :event_date_id
      t.string :season_desc
      t.string :season_code
      t.string :summer_season_flag
      t.string :summer_season_desc
      t.string :summer_season_code

      t.timestamps
    end
  end
end
