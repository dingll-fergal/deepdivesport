class CreateTeamFacts < ActiveRecord::Migration[5.0]
  def change
    create_table :team_facts do |t|
      t.integer :team_dim_id
      t.integer :competition_id
      t.integer :league_position
      t.integer :played
      t.integer :win
      t.integer :draw
      t.integer :loss
      t.integer :points
      t.integer :goals_for
      t.integer :goals_against
      t.integer :goal_diff
      t.integer :home_win
      t.integer :home_draw
      t.integer :home_loss
      t.integer :home_points
      t.integer :home_goals_for
      t.integer :home_goals_against
      t.integer :home_goal_diff
      t.integer :away_win
      t.integer :away_loss
      t.integer :away_draws
      t.integer :away_points
      t.integer :away_goals_for
      t.integer :away_goal_against
      t.integer :away_goal_diff

      t.timestamps
    end
  end
end
