class CreateArticles < ActiveRecord::Migration[5.0]
  def change
    create_table :articles do |t|
      t.integer :user_id
      t.integer :tournament_id
      t.integer :event_id
      t.string :description

      t.timestamps
    end
  end
end
