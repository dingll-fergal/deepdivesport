class CreateEventTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :event_types do |t|
      t.string :type_desc
      t.integer :sport_id
      t.integer :competition_id
      t.integer :country_id
      t.integer :duration
      t.boolean :completed
      t.boolean :abandoned

      t.timestamps
    end
  end
end
