class CreateTeamPlayers < ActiveRecord::Migration[5.0]
  def change
    create_table :team_players do |t|
      t.integer :team_dim_id
      t.integer :player_dim_id

      t.timestamps
    end
  end
end
