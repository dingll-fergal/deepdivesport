class CreateTournaments < ActiveRecord::Migration[5.0]
  def change
    create_table :tournaments do |t|
      t.integer :sport_id
      t.string :name
      t.integer :country_id

      t.timestamps
    end
  end
end
