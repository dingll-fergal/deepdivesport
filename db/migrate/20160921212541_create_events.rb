class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.string :description
      t.integer :event_date_id
      t.integer :event_type_id
      t.integer :event_venue_id
      t.integer :competition_id
      t.integer :home_team_id
      t.integer :away_team_id
      t.integer :fixture_id

      t.timestamps
    end
  end
end
