class CreateEventDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :event_details do |t|
      t.string :event_type
      t.integer :team_1_id
      t.integer :team_2_id
      t.integer :event_id

      t.timestamps
    end
  end
end
