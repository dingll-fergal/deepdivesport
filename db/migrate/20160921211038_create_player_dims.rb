class CreatePlayerDims < ActiveRecord::Migration[5.0]
  def change
    create_table :player_dims do |t|

      t.string :player_name
      t.datetime :date_of_birth
      t.integer :age
      t.integer :country_id
      t.boolean :retired
      t.datetime :retirement_date
      t.integer :first_club_id
      t.integer :current_club_id

      t.timestamps
    end
  end
end
