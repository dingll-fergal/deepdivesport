class CreateTeamDims < ActiveRecord::Migration[5.0]
  def change
    create_table :team_dims do |t|
      t.string :team_name
      t.integer :competition_id
      t.string :address1
      t.string :address2
      t.string :address3
      t.integer :post_code
      t.string :home_ground
      t.integer :country_id
      t.boolean :current_flag

      t.timestamps
    end
  end
end
