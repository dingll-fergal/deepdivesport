class CreateMemberProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :member_profiles do |t|
      t.string :about
      t.string :phone
      t.boolean :is_profile_public
      t.string :photo_url

      t.timestamps
    end
  end
end
