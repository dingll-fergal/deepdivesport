class CreateVideos < ActiveRecord::Migration[5.0]
  def change
    create_table :videos do |t|
      t.integer :user_id
      t.integer :mediable_id
      t.string :mediable_type
      t.string :video_url

      t.timestamps
    end
  end
end
