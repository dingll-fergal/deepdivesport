class CreateFanCards < ActiveRecord::Migration[5.0]
  def change
    create_table :fan_cards do |t|
      t.string :title
      t.string :description
      t.integer :fanzoned_id
      t.string :fanzoned_type

      t.timestamps
    end
  end
end
