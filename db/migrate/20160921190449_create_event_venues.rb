class CreateEventVenues < ActiveRecord::Migration[5.0]
  def change
    create_table :event_venues do |t|
      t.string :venue_name
      t.integer :home_team_id
      t.string :home_team_name
      t.string :address1
      t.string :address2
      t.string :address3
      t.integer :country_id
      t.string :city
      t.integer :postcode
      t.string :phone
      t.integer :capacity
      t.string :owner

      t.timestamps
    end
  end
end
