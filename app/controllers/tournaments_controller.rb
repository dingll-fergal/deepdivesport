class TournamentsController < ApplicationController
  def tournament_search

  end

  def show
    @tournament = Tournament.find(params[:id])
    @event = Event.where(tournament_id: params[:id]).pluck(:id)
  end
end
