class CommentsController < ApplicationController

  def index
  end

  def new
  end

  def create

  if params[:type] == 'event'
      event = EventType.find(params[:id])
      comment = event.comments.build(user_params)
      comment.user_id = current_user.id
      comment.save!
    elsif params[:type] == 'fancard'
      fan_card = FanCard.find(params[:id])
      comment = fan_card.comments.build(user_params)
      comment.user_id = current_user.id
      comment.save!
    end

    redirect_to :back

  end

  private

  def comment_params
    params.require(:comment).permit(:description)
  end
end
