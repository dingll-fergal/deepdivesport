class FavouritesController < ApplicationController


  def index
    @sports = Sport.all
    @teams  = TeamDim.all
  end

  def create
    params[:sports].each do |sport_id|
      sport       = Sport.find_by_id(sport_id.to_i)
      fav         = sport.favourites.build
      fav.user_id = current_user.id
      fav.save
    end
    if params[:teams].present?
      params[:teams].each do |team_id|
        team        = TeamDim.find_by_id(team_id.to_i)
        fav         = team.favourites.build
        fav.user_id = current_user.id
        fav.save
      end
    end
    flash[:success] = "Favourites Successfully"
    redirect_to new_favourite_path
  end

  def new
    @favourite_teams        = current_user.favourites.where(favouriteable_type: 'TeamDim')
    fav_team_ids            = @favourite_teams.pluck(:favouriteable_id)
    @favourite_teams        = TeamDim.where('id IN (?) ', fav_team_ids)

    @favourite_sports       = current_user.favourites.where(favouriteable_type: 'Sport')
    fav_sport_ids           = @favourite_sports.pluck(:favouriteable_id)
    @favourite_competitions = Competition.where('sport_id IN (?) ', fav_sport_ids)
    competition_ids         = Competition.where('sport_id IN (?) ', fav_sport_ids).pluck(:id)
    @favourite_events       = Event.where('competition_id IN (?) ', competition_ids)
  end

  def show
  end

end
