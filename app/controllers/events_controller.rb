class EventsController < ApplicationController
  include ApplicationHelper

  def show
    @event_show        = Event.find(params[:id])
    @fan_cards         = @event_show.fan_cards
    @event_comments    = Comment.where(commentable_id: params[:id],commentable_type: 'Event' )
    @articles          = Article.where(event_id: params[:id])
    @comment = Comment.new
    @fan_card = FanCard.new
    team_show()
    player_show()
  end

end
