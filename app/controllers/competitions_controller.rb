class CompetitionsController < ApplicationController
  include ApplicationHelper

  def show

    @competition = Competition.find(params[:id])
    @event = Event.where(competition_id: params[:id]).pluck(:id)
    @competition_comments    = Comment.where(commentable_id: params[:id],commentable_type: 'Competition' )
    @article          = Article.where(competition_id: params[:id])
    @comment = Comment.new
    @like = Like.new
    team_show()
    player_show()
  end
end
