class Article < ApplicationRecord
  belongs_to :competition
  belongs_to :event
end
