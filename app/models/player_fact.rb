class PlayerFact < ApplicationRecord
  belongs_to :player_dim
  belongs_to :competition
  has_many :team_players
end
