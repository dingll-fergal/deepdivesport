class Fixture < ApplicationRecord
  belongs_to :event_date
  belongs_to :competition
  belongs_to :event_venue
  has_many :events
end
