class Video < ApplicationRecord
  belongs_to :mediable, polymorphic: true
end
