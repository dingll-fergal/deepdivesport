class PlayerDim < ApplicationRecord
  has_many :team_dims ,through: :team_players
  belongs_to :country
  has_one :player_fact
end
