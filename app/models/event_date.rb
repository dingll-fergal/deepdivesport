class EventDate < ApplicationRecord
  belongs_to :competitions
  has_many :fixtures
end
