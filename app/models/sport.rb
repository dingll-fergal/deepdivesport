class Sport < ApplicationRecord
  has_many :favourites, as: :favouriteable
  has_many :tournaments
  has_many :teams
  has_many :event_types
  has_many :competitions
  accepts_nested_attributes_for :favourites
end
