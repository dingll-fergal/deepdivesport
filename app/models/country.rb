class Country < ApplicationRecord
  has_many :teams
  has_many :member_profiles
  has_many :admin_profiles
  has_many :event_venues
  has_many :player_dims

end
