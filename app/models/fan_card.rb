class FanCard < ApplicationRecord
  belongs_to :fanzoned, polymorphic: true
  belongs_to :comments, as: :commentable
end
