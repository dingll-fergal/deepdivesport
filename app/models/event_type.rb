class EventType < ApplicationRecord
  belongs_to :sport
  belongs_to :competition
  belongs_to :country
end
