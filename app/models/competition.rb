class Competition < ApplicationRecord
  belongs_to :event_date
  has_many :sport
  has_one :fixture
  has_many :event_types
  has_many :team_facts
  has_many :player_facts
  has_many :articles
  has_many :likes, as: :likeable
  has_many :comments, as: :commentable


end
