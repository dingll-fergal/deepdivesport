class Event < ApplicationRecord
  belongs_to :event_date
  belongs_to :event_type
  belongs_to :event_venue
  belongs_to :competition
  belongs_to :fixture
  has_many :likes, as: :likeable
  has_many :comments, as: :commentable
  has_many :fan_cards, as: :fanzoned
  has_many :articles
  has_many :photos, as: :imageable
  has_many :videos, as: :mediable
end
