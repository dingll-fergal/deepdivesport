class Team < ApplicationRecord
  has_many :favourites, as: :favouriteable
  has_many :tournament_teams
  has_many :tournaments , through: :tournament_teams
  belongs_to :sport
  belongs_to :country
  accepts_nested_attributes_for :favourites
end
