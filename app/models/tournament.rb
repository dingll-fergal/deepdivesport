class Tournament < ApplicationRecord
  has_many :favourites, as: :favouriteable
  has_many :likes, as: :likeable
  has_many :comments, as: :commentable
  has_many :photos, as: :imageable
  has_many :fan_cards, as: :fanzoned
  has_many :videos, as: :mediable
  has_many :events
  has_many :tournament_teams
  has_many :articles
  has_many :teams, through: :tournament_teams
  belongs_to :sport
  accepts_nested_attributes_for :favourites

  #
  # pg_search_scope :search_by_title,
  #                 :against => :name,
  #                 :using => {
  #                     :tsearch=>{
  #                         :any_word => true,
  #                         :dictionary => "english"
  #                     }
  #                 }

end
