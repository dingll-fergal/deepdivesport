class AdminProfile < ApplicationRecord
  has_one :user, as: :profile
  belongs_to :country
  accepts_nested_attributes_for :user
end
