class EventVenue < ApplicationRecord
  belongs_to :country
  has_many :fixtures
end
