class TeamDim < ApplicationRecord
  has_many :favourites, as: :favouriteable
  has_many :team_players
  has_many :player_dims , through: :team_players
  belongs_to :country
  belongs_to :competition
  has_one :team_fact
end
