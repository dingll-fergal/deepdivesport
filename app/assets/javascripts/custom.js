
$(document).ready(function () {
    $('#blog-carousile').carousel({
        interval: 10000
    })
    $('.fdi-Carousel .item').each(function () {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        if (next.next().length > 0) {
            next.next().children(':first-child').clone().appendTo($(this));
        }
        else {
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
        }
    });
});




$(document).ready(function () {
    $( ".chat-right-hover-panel" ).click(function() {
        $( ".right-tab-container-wrapper" ).toggleClass("display-block" );
    });

    $( ".mobile-left-hover-panel" ).click(function() {
        $( ".dashboard-left-inner-wrapper" ).toggleClass("display-block" );
    });

    $( ".search-button" ).click(function() {
        $( ".heading-panel-search input" ).toggleClass("heading-panel-search-place" );
    });

    $( ".about-panel" ).click(function() {
        $( ".information-panel-container" ).toggleClass("display-block" );
    });

    $( ".header-search-bar a" ).click(function() {
        $( ".header-search-bar input" ).toggleClass("display-inline-block" );
    });

    $( ".new-card-popup-button a" ).click(function() {
        $( ".new-card-popup-button .popup" ).toggleClass("display-block" );
    });

    $( ".close" ).click(function() {
        $( ".new-card-popup-button .popup" ).toggleClass("display-block" );
    });

    $( ".user-ul" ).click(function() {
        $( ".user-setting-data" ).toggleClass("display-block" );
    });

    $( ".invite-button" ).click(function() {
        $( ".invite-popup" ).toggleClass("display-block" );
    });

    $( ".close-1" ).click(function() {
        $( ".invite-popup " ).toggleClass("display-block" );
    });

    //  $( ".invite-button" ).click(function() {
    //  $( ".invite-button" ).toggleClass("display-block" );
    // });



    //    $( ".table-cell-panel" ).click(function() {
    //  $( ".invite-popup " ).toggleClass("display-block" );
    // });




});
