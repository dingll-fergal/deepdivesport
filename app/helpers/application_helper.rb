module ApplicationHelper
  def team_show
    teamdim=[]
    @team_dim = TeamDim.where(competition_id: params[:id]).pluck(:id)
    @team_dim.each do |s|
      @teams = TeamDim.find_by_id(s)
      teamdim << @teams
    end
    @teamdim_arr = teamdim
  end
  def player_show
    @home_team         = Event.where(id: params[:id]).pluck(:home_team_id)
    @away_team         = Event.where(id: params[:id]).pluck(:away_team_id)
    @team_1            = TeamDim.where(id: @home_team)
    @team_2            = TeamDim.where(id: @away_team)
    @home_teams_player = TeamPlayer.where(team_dim_id: @home_team)
    @home_players      = PlayerDim.where(id: @home_teams_player)
    @away_teams_player = TeamPlayer.where(team_dim_id: @away_team)
    @away_players      = PlayerDim.where(id: @away_teams_player)
  end
end
